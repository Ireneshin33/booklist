$(document).ready(function() {
    var book1 = {
        title: "Good Economics for Hard Times,",
        author: "Abhijit Banerjee and Esther Duflo",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Good-Economics-for-Hard-Times/summer-books_2020_good-economics-for-hard-times_article-hero_1200x564_02.ashx",
        abstract: "Banerjee and Duflo won the Nobel Memorial Prize in Economic Sciences last year, and they're two of the smartest economists working today. Fortunately for us, they're also very good at making economics accessible to the average person. Their newest book takes on inequality and political divisions by focusing on policy debates that are at the forefront in wealthy countries like the United States.",
        year: "2020"
    };

    var book2 = {
        title: "The Ride of a Lifetime",
        author: "Bob Iger.",
        image: "https://media.gatesnotes.com/-/media/Images/Books/The-Ride-of-a-Lifetime/summer-books_2020_the-ride-of-a-lifetime_article-hero_1200x564_01.ashx",
        abstract: "This is one of the best business books I've read in several years. Iger does a terrific job explaining what it's really like to be the CEO of a large company. Whether you're looking for business insights or just an entertaining read, I think anyone would enjoy his stories about overseeing Disney during one of the most transformative times in its history.",
        year: "2020"


    };

    var book3 = {
        title: "Cloud Atlas",
        author: "David Mitchell",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Cloud-Atlas/summer-books_2020_cloud-atlas_article-hero_1200x564_01.ashx",
        abstract: "This is the kind of novel you'll think and talk about for a long time after you finish it. The plot is a bit hard to explain, because it involves six inter-related stories that take place centuries apart (including one I particularly loved about a young American doctor on a sailing ship in the South Pacific in the mid-1800s). But if you're in the mood for a really compelling tale about the best and worst of humanity, I think you'll find yourself as engrossed in it as I was.",
        year: "2020"

    };

    var book4 = {
        title: "The Choice",
        author: "Dr. Edith Eva Eger",
        image: "https://media.gatesnotes.com/-/media/Images/Books/The-Choice/summer-books_2020_the-choice_article-hero_1200x564_01.ashx",
        abstract: "This book is partly a memoir and partly a guide to processing trauma. Eger was only sixteen years old when she and her family got sent to Auschwitz. After surviving unbelievable horrors, she moved to the United States and became a therapist. Her unique background gives her amazing insight, and I think many people will find comfort right now from her suggestions on how to handle difficult situations.",
        year: "2020"

    };

    var book5 = {
        title: "The Great Influenza",
        author: "John M. Barry",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Health/The-Great-Influenza/summer-books_2020_the-great-influenza_article-hero_1200x564_01.ashx",
        abstract: "We're living through an unprecedented time right now. But if you're looking for a historical comparison, the 1918 influenza pandemic is as close as you're going to get. Barry will teach you almost everything you need to know about one of the deadliest outbreaks in human history. Even though 1918 was a very different time from today, The Great Influenza is a good reminder that we're still dealing with many of the same challenges.",
        year: "2020"


    };

    var books = new Array();
    books.push(book1);
    books.push(book2);
    books.push(book3);
    books.push(book4);
    books.push(book5);


    $('ol').addClass("list-group");
    $('li').addClass("list-group-item");

    $('li').each(function(i) {
        let str = '"' + books[i].title + '"' + ' by ' + books[i].author;
        let imageUrl = books[i].image;
        let abs = books[i].abstract
        let image = '<img src="' + imageUrl + '">' + str + '<br>' + abs;
        $(this).html(image);
    });

    $('li').each(function(i) {
        if (i % 2 != 0) {
            $(this).addClass("even");
        } else {
            $(this).addClass("odd");
        }
    });

});