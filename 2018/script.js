$(document).ready(function() {
    var book1 = {
        title: "Everything Happens for a Reason and Other Lies I've Loved",
        author: "Kate Bowler",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Everything-Happens-for-a-Reason/summer-books_2018_happens-for-a-reason_1200px_v1.ashx",
        abstract: "When Bowler, a professor at Duke Divinity School, is diagnosed with stage IV colon cancer, she sets out to understand why it happened. Is it a test of her character? The result is a heartbreaking, surprisingly funny memoir about faith and coming to grips with your own mortality.",
        year: "2018"
    };
    var book2 = {
        title: "Leonardo da Vinci",
        author: "Walter Isaacson ",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Leonardo-da-Vinci/summer-books_2018_da-vinci_1200px_v1.ashx",
        abstract: "I think Leonardo was one of the most fascinating people ever. Although today he's best known as a painter, Leonardo had an absurdly wide range of interests, from human anatomy to the theater. Isaacson does the best job I've seen of pulling together the different strands of Leonardo's life and explaining what made him so exceptional. A worthy follow-up to Isaacson's great biographies of Albert Einstein and Steve Jobs.",

        year: "2018"
    };
    var book3 = {
        title: "Origin Story: A Big History of Everything",
        author: "David Christian",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Origin-Story/summer-books_2018_origin-story_1200px_v1.ashx",
        abstract: "David created my favorite course of all time, Big History. It tells the story of the universe from the big bang to today's complex societies, weaving together insights and evidence from various disciplines into a single narrative. If you haven't taken Big History yet, Origin Story is a great introduction. If you have, it's a great refresher. Either way, the book will leave you with a greater appreciation of humanity's place in the universe.",
        year: "2018"



    };
    var book4 = {
        title: "Factfulness",
        author: "Hans Rosling, with Ola Rosling and Anna Rosling Ronnlund",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Factfulness/factfulness_2018_article-hero_1200px_v2.ashx",
        abstract: "I've been recommending this book since the day it came out. Hans, the brilliant global-health lecturer who died last year, gives you a breakthrough way of understanding basic truths about the world'how life is getting better, and where the world still needs to improve. And he weaves in unforgettable anecdotes from his life. It's a fitting final word from a brilliant man, and one of the best books I've ever read.",



        year: "2018"
    };
    var book5 = {
        title: "Lincoln in the Bardo",
        author: "George Saunders",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Lincoln-in-the-Bardo/summer-books_2018_lincoln_1200px_v1.ashx",

        abstract: "I thought I knew everything I needed to know about Abraham Lincoln, but this novel made me rethink parts of his life. It blends historical facts from the Civil War with fantastical elements it's basically a long conversation among 166 ghosts, including Lincoln's deceased son. I got new insight into the way Lincoln must have been crushed by the weight of both grief and responsibility. This is one of those fascinating, ambiguous books you'll want to discuss with a friend when you're done.",


        year: "2018"
    };

    var books = new Array();
    books.push(book1);
    books.push(book2);
    books.push(book3);
    books.push(book4);
    books.push(book5);


    $('ol').addClass("list-group");
    $('li').addClass("list-group-item");

    $('li').each(function(i) {
        let str = '"' + books[i].title + '"' + ' by ' + books[i].author;
        let imageUrl = books[i].image;
        let abs = books[i].abstract
        let image = '<img src="' + imageUrl + '">' + str + '<br>' + abs;
        $(this).html(image);
    });

    $('li').each(function(i) {
        if (i % 2 != 0) {
            $(this).addClass("even");
        } else {
            $(this).addClass("odd");
        }
    });

});