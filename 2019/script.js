$(document).ready(function() {
    var book1 = {
        title: "Upheaval",
        author: "Jared Diamond",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Upheaval/Summer-Books_Blog-Heros_final_04_Upheaval.ashx",
        abstract: "I'm a big fan of everything Jared has written, and his latest is no exception. The book explores how societies react during moments of crisis. He uses a series of fascinating case studies to show how nations managed existential challenges like civil war, foreign threats, and general malaise. It sounds a bit depressing, but I finished the book even more optimistic about our ability to solve problems than I started.",
        year: "2019"
    };

    var book2 = {
        title: "A Gentleman in Moscow",
        author: "Amor Towles. ",
        image: "https://media.gatesnotes.com/-/media/Images/Books/A-Gentleman-in-Moscow/Summer-Books_Blog-Heros_final_01_Moscow.ashx",
        abstract: "It seems like everyone I know has read this book. I finally joined the club after my brother-in-law sent me a copy, and I'm glad I did. Towles's novel about a count sentenced to life under house arrest in a Moscow hotel is fun, clever, and surprisingly upbeat. Even if you don't enjoy reading about Russia as much as I do (I've read every book by Dostoyevsky), A Gentleman in Moscow is an amazing story that anyone can enjoy.",
        year: "2019"
    };

    var book3 = {
        title: "Nine Pints",
        author: "Rose George",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Health/Nine-Pints/Summer-Books_Blog-Heros_final_05_NinePints.ashx",
        abstract: "If you get grossed out by blood, this one probably isn't for you. But if you're like me and find it fascinating, you'll enjoy this book by a British journalist with an especially personal connection to the subject. I'm a big fan of books that go deep on one specific topic, so Nine Pints (the title refers to the volume of blood in the average adult) was right up my alley. It's filled with super-interesting facts that will leave you with a new appreciation for blood.",
        year: "2019"
    };

    var book4 = {
        title: "Presidents of War",
        author: "Michael Beschloss. ",
        image: "https://media.gatesnotes.com/-/media/Images/Books/Presidents-of-War/Summer-Books_Blog-Heros_final_02_POW.ashx",
        abstract: "My interest in all aspects of the Vietnam War is the main reason I decided to pick up this book. By the time I finished it, I learned a lot not only about Vietnam but about the eight other major conflicts the U.S. entered between the turn of the 19th century and the 1970s. Beschloss's broad scope lets you draw important cross-cutting lessons about presidential leadership.",
        year: "2019"
    };

    var book5 = {
        title: "The Future of Capitalism",
        author: "Paul Collier",
        image: "https://media.gatesnotes.com/-/media/Images/Books/The-Future-of-Capitalism/Summer-Books_Blog-Heros_final_03_Capitalism.ashx",
        abstract: "Collier's latest book is a thought-provoking look at a topic that's top of mind for a lot of people right now. Although I don't agree with him about everything. I think his analysis of the problem is better than his proposed solutions his background as a development economist gives him a smart perspective on where capitalism is headed.",
        year: "2019"
    };

    var books = new Array();
    books.push(book1);
    books.push(book2);
    books.push(book3);
    books.push(book4);
    books.push(book5);


    $('ol').addClass("list-group");
    $('li').addClass("list-group-item");

    $('li').each(function(i) {
        let str = '"' + books[i].title + '"' + ' by ' + books[i].author;
        let imageUrl = books[i].image;
        let abs = books[i].abstract
        let image = '<img src="' + imageUrl + '">' + str + '<br>' + abs;
        $(this).html(image);
    });

    $('li').each(function(i) {
        if (i % 2 != 0) {
            $(this).addClass("even");
        } else {
            $(this).addClass("odd");
        }
    });

});