$(document).ready(function() {
    const base_url = "https://billgatesbooks.wl.r.appspot.com";
    const url2018 = base_url + " https://billgatesbooks.wl.r.appspot.com/2018";
    const url2019 = base_url + " https://billgatesbooks.wl.r.appspot.com/2019";
    const url2020 = base_url + "https://billgatesbooks.wl.r.appspot.com/2020";


    $("#2018").on("click", function(_e) {
        $("table tr td").each(function() {
            $("table").empty();
        });

        $.ajax({
            url: url2018,
            dataType: "json",
            success: function(_BuildTable, data, allofData) {
                var allofData = data["2018"];
                $.fn.BuildTable(data, allofData);
            }
        });
    });



    $("#2019").on("click", function(e) {
        $("table tr td").each(function() {
            $("table").empty();
        });

        $.ajax({
            url: url2019,
            dataType: "json",
            success: function(_BuildTable, data, allofData) {
                var allofData = data["2019"];
                $.fn.BuildTable(data, allofData);
            }
        });
    });

    $("#2020").on("click", function(e) {
        $("table tr td").each(function() {
            $("table").empty();
        });

        $.ajax({
            url: url2020,
            dataType: "json",
            success: function(_BuildTable, data, allofData) {
                var allofData = data["2020"];
                $.fn.BuildTable(data, allofData);
            }
        });
    });

    $.fn.buildTable = function(data, allofData) {
        $("table").addClass("table");

        let Headeroftable =
            "<thead><tr><th   scope='col'>Title</th><th           scope='col' class='author'>Author</th><th scope='col'class='abstract'>Abstract</th></thead>";
        $("table").append(Headeroftable);
        for (i = 0; i < allofData.lenght; i++) {
            var str =
                "<tr><td><figure><img src='" +
                allofData[i].image +
                "''><figcaption>" +
                allofData[i].title +
                "</figcaption></figure></td><td class='author'>" +
                allofData[i].author +
                "</td><td class='abstract text-justify'>" +
                allofData[i].abstract +
                "</td></tr>";
            $("td").addClass("align-middle");
            $("table").append(str);
        }
    };
});